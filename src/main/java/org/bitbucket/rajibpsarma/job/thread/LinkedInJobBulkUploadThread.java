package org.bitbucket.rajibpsarma.job.thread;

import java.util.ArrayList;
import java.util.List;

import org.bitbucket.rajibpsarma.common.Constants.EntityType;
import org.bitbucket.rajibpsarma.common.RecordVOI;
import org.bitbucket.rajibpsarma.common.thread.LinkedInBulkUploadThread;
import org.bitbucket.rajibpsarma.job.vo.JobVO;
import org.springframework.stereotype.Component;

@Component
public class LinkedInJobBulkUploadThread extends LinkedInBulkUploadThread {

	@Override
	public EntityType getEntityType() {
		return EntityType.JOB;
	}

	@Override
	public List<RecordVOI> getNextRecords(long lastEntityId) {
		List<RecordVOI> list = new ArrayList<RecordVOI>();
		
		list.add(new JobVO(1, "Job 1"));
		list.add(new JobVO(2, "Job 2"));
		list.add(new JobVO(3, "Job 3"));
		
		return list;
	}

}
