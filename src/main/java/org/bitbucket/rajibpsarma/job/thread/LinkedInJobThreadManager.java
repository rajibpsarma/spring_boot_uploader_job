package org.bitbucket.rajibpsarma.job.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.bitbucket.rajibpsarma.JobConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class LinkedInJobThreadManager implements ApplicationRunner {
	private ExecutorService service;
	
	@Autowired
	private LinkedInJobBulkUploadThread jobThread;
	
	public void switchOn() {
		System.out.println("*** Switching on Job Thread Manager ...");
		if((service == null) || (service.isShutdown())) {
			service = Executors.newFixedThreadPool(1);
		}
		
		service.execute(jobThread);
	}
	
	public void switchOff() {
		if(service != null) {
			service.shutdown();
		}
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(JobConfig.class);
		LinkedInJobThreadManager mgr = ctx.getBean(LinkedInJobThreadManager.class, "linkedInJobThreadManager");
		mgr.switchOn();
	}
}
