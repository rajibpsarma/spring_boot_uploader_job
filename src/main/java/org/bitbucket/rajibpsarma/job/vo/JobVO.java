package org.bitbucket.rajibpsarma.job.vo;

import org.bitbucket.rajibpsarma.common.RecordVOI;

public class JobVO implements RecordVOI {
	private long recordID;
	private String job;
	@Override
	public long getRecordID() {
		return recordID;
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public String getErrorMsg() {
		return null;
	}

	@Override
	public void setRecordID(long recordID) {
		this.recordID = recordID;
	}

	@Override
	public void setErrorMsg(String errorMsg) {
	}
	public JobVO(long recordID, String job) {
		this.recordID = recordID;
		this.job = job;
	}
	public String toString() {
		return "{recordId:"+recordID + ", job:" + job + "}";
	}
}
