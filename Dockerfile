FROM java:8

# Set the working directory.
WORKDIR /usr/src/app/uploader_job

# Copy the file required
COPY ./target/spring_boot_uploader_job.jar spring_boot_uploader_job.jar

# Inform Docker that the container is listening on the specified port at runtime.
EXPOSE 8080

# Run the specified command within the container.
# i.e. java -jar spring-boot-app.jar
CMD ["java", "-jar", "spring_boot_uploader_job.jar"]
