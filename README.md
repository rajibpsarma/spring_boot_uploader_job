# spring_boot_uploader_job #

It's a Spring Boot App that uses the common code in project 
[spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)

### Steps (only testing steps) : ###
* Build the jar using "mvn clean package"
* Build a docker image "uploader_job" using command "sudo docker build -t uploader_job ."
* Create a container, publish 8080 port of host to 8080 of container, using "sudo docker run -d -p 8080:8080 --name uploader_job uploader_job".
* The REST end point is available at "http://localhost:8080/rest/jobs"

#### The following projects are part of a system that works together : ####
* [spring_boot_uploader_libs](https://bitbucket.org/rajibpsarma/spring_boot_uploader_libs)
* [spring_boot_uploader_job](https://bitbucket.org/rajibpsarma/spring_boot_uploader_job)
* [spring_boot_uploader_cand](https://bitbucket.org/rajibpsarma/spring_boot_uploader_cand)
* [uploader-ui](https://bitbucket.org/rajibpsarma/uploader-ui)
